package main

import (
	"encoding/json"
	"fmt"
	"github.com/rs/cors"
	. "lang.yottadb.com/go/yottadb"
	"net/http"
	"os"
	"runtime"
	"strconv"
	"time"
  "math"
)

type outputValue struct {
  Key   float64 `json:"x"`
  Value int `json:"y"`
  Heat int `json:"value"`
}

type outputStruct struct {
	Hostname   string
	Arch       string
	Start_time string
	End_time   string
	Values     []outputValue
}

func noErr(err error) {
	if err != nil {
		panic(err)
	}
}

func main() {
	mux := http.NewServeMux()

  fs := http.FileServer(http.Dir("static"))
  mux.Handle("/static/", http.StripPrefix("/static/", fs))
	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		// Allocate a buffer to scan through ^result
		var results KeyT
		var resultsXref KeyT
		var errstr, next, value BufferT
    var tempBufferArray *BufferTArray

		tptoken := NOTTP

		errstr.Alloc(128)
		next.Alloc(128)
		value.Alloc(128)
		results.Alloc(32, 12, 1024)
		resultsXref.Alloc(32, 12, 1024)

		results.Varnm.SetValStrLit(tptoken, &errstr, "^step")
		results.Subary.SetValStrLit(tptoken, &errstr, 0, "")
		results.Subary.SetElemUsed(tptoken, &errstr, 1)

		resultsXref.Varnm.SetValStrLit(tptoken, &errstr, "^stepXref")
		resultsXref.Subary.SetElemUsed(tptoken, &errstr, 2)

		hostname, err := os.Hostname()
		noErr(err)

		start_time := "0"
		start_time_t, ok := req.URL.Query()["start_time"]
		if ok {
			start_time = start_time_t[0]
		}

		end_time := fmt.Sprintf("%d", time.Now().UnixNano() / int64(time.Millisecond))
		end_time_t, ok := req.URL.Query()["end_time"]
		if ok {
			end_time = end_time_t[0]
		}

		output := outputStruct{
			Hostname:   hostname,
			Arch:       runtime.GOARCH,
			Start_time: start_time,
			End_time:   end_time,
			Values:     []outputValue{},
		}

		resultsXref.Subary.SetValStr(tptoken, &errstr, 0, &start_time)
		resultsXref.Subary.SetValStrLit(tptoken, &errstr, 1, "0")

		for {
      tempBufferArray = new(BufferTArray)
      tempBufferArray.Alloc(4, 1024)
			err := resultsXref.NodeNextST(tptoken, &errstr, tempBufferArray)
			if err != nil {
				errcode := ErrorCode(err)
				if errcode == YDB_ERR_NODEEND {
					break
				}
				panic(err)
			}
      resultsXref.Subary = tempBufferArray
			cur_time, err := resultsXref.Subary.ValStr(tptoken, &errstr, 0)
			if *cur_time == end_time {
				break
			}
			next_str, err := resultsXref.Subary.ValStr(tptoken, &errstr, 1)
			results.Subary.SetValStr(tptoken, &errstr, 0, next_str)
			results.ValST(tptoken, &errstr, &value)
			value, err := value.ValStr(tptoken, &errstr)
			if err != nil {
				panic(err)
			}
			key, err := strconv.Atoi(*next_str)
			noErr(err)
			val, err := strconv.Atoi(*value)
			noErr(err)
			new_value := outputValue{
				Key:   math.Pow(float64(key), 1.0/3.0),
				Value: val * 5,
        Heat: 1,
			}
			output.Values = append(output.Values, new_value)
		}

		js, err := json.Marshal(output)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Set("Content-Type", "application/json")
		w.Write(js)
	})


	handler := cors.Default().Handler(mux)
	http.ListenAndServe(":3000", handler)
}
